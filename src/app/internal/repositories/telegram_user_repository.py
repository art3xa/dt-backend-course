from asgiref.sync import sync_to_async

from app.internal.models.telegram_user import TelegramUser


class TelegramUserRepository:

    @sync_to_async
    def create(self, telegram_id: int, username: str) -> TelegramUser:
        obj, created = TelegramUser.objects.update_or_create(telegram_id=telegram_id, username=username)
        return obj

    def get(self, telegram_id: int) -> TelegramUser | None:
        return TelegramUser.objects.filter(telegram_id=telegram_id).first()

    @sync_to_async
    def update_phone_number(self, telegram_user: TelegramUser, phone_number: str) -> TelegramUser:
        telegram_user.phone_number = phone_number
        telegram_user.save(update_fields=["phone_number"])
        return telegram_user
