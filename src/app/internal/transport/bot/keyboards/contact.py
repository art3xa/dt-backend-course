from aiogram.types import KeyboardButton, ReplyKeyboardMarkup


def get_contact_keyboard() -> ReplyKeyboardMarkup:
    first_button = KeyboardButton(text="📱 Send", request_contact=True)
    markup = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[[first_button]])
    return markup
