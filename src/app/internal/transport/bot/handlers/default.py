from aiogram import F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, ReplyKeyboardRemove

from app.internal.repositories.telegram_user_repository import TelegramUserRepository
from app.internal.services.telegram_user_service import TelegramUserService
from app.internal.transport.bot.keyboards.contact import get_contact_keyboard
from app.internal.transport.bot.replies import Replies, get_info_about_telegram_user

default_router = Router()


def get_tg_user_service() -> TelegramUserService:
    tg_user_repo = TelegramUserRepository()
    return TelegramUserService(tg_user_repo=tg_user_repo)


@default_router.message(Command("start"))
async def start(msg: Message, state: FSMContext, tg_user_service: TelegramUserService = get_tg_user_service()):
    await tg_user_service.create(telegram_id=msg.from_user.id, username=msg.from_user.username)
    await state.set_state(None)
    await msg.answer(Replies.START)


@default_router.message(Command("help"))
async def help(msg: Message):
    await msg.answer(Replies.HELP)


@default_router.message(Command("me"))
async def me(msg: Message, tg_user_service: TelegramUserService = get_tg_user_service()):
    telegram_user = await tg_user_service.aget(telegram_id=msg.from_user.id)
    if telegram_user is None:
        await msg.answer(Replies.USER_DOES_NOT_EXIST)
        return
    if telegram_user.phone_number is None:
        await msg.answer(Replies.DONT_HAVE_ACCESS_TO_COMMAND)
        return
    await msg.answer(get_info_about_telegram_user(telegram_user))


@default_router.message(Command("set_phone"))
async def set_phone_number(msg: Message, tg_user_service: TelegramUserService = get_tg_user_service()):
    telegram_user = await tg_user_service.aget(telegram_id=msg.from_user.id)
    if telegram_user is None:
        await msg.answer(Replies.USER_DOES_NOT_EXIST)
        return
    await msg.answer(Replies.PRESS_BUTTON_TO_SEND_PHONE_NUMBER, reply_markup=get_contact_keyboard())


@default_router.message(F.contact)
async def get_phone_number(msg: Message, tg_user_service: TelegramUserService = get_tg_user_service()):
    if msg.contact.phone_number is None:
        await msg.answer(Replies.PHONE_NUMBER_WAS_NOT_RECEIVED)
        return
    telegram_user = await tg_user_service.aget(telegram_id=msg.from_user.id)
    if telegram_user is None:
        await msg.answer(Replies.USER_DOES_NOT_EXIST)
        return
    await tg_user_service.update_phone_number(telegram_user, phone_number=msg.contact.phone_number)
    await msg.answer(
        Replies.PHONE_NUMBER_SAVED,
        reply_markup=ReplyKeyboardRemove(),
    )
