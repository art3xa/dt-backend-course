import enum

from app.internal.models.telegram_user import TelegramUser


class Replies(enum.StrEnum):
    START = "Hello!\nUser created.\nWrite /help for see help\nWrite /set_phone for set phone number"
    HELP = "/start - start\n/help - help\n/set_phone - set phone number\n/me - get info about me"
    USER_DOES_NOT_EXIST = "User does not exists. Write /start to create user"
    PRESS_BUTTON_TO_SEND_PHONE_NUMBER = "Click on the button below to send a contact"
    PHONE_NUMBER_SAVED = "Your phone number saved"
    PHONE_NUMBER_WAS_NOT_RECEIVED = "The phone number was not received"
    DONT_HAVE_ACCESS_TO_COMMAND = "You dont have access to use this command. Send phone /set_phone"


def get_info_about_telegram_user(telegram_user: TelegramUser) -> str:
    return (
        f"Telegram ID: {telegram_user.telegram_id}\n"
        f"Username: {telegram_user.username}\n"
        f"Phone Number: {telegram_user.phone_number}"
    )
