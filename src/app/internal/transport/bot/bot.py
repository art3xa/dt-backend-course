from aiogram import Bot, Dispatcher
from aiogram.enums import ParseMode
from aiogram.fsm.storage.memory import MemoryStorage

from app.internal.transport.bot.handlers.default import default_router


class TelegramBot:
    def __init__(self, token: str) -> None:
        self.token = token
        self.app = Bot(token=self.token, parse_mode=ParseMode.HTML)
        self.storage = MemoryStorage()
        self.dp = Dispatcher(storage=self.storage)

    def add_routers(self) -> None:
        self.dp.include_router(default_router)

    async def start(self) -> None:
        await self.app.delete_webhook(drop_pending_updates=True)
        await self.dp.start_polling(self.app, allowed_updates=self.dp.resolve_used_update_types())
