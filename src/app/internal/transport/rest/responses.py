from django.http import JsonResponse


class Responses:
    ACCESS_DENIED = JsonResponse({"error": "access denied"})
    USER_DOES_NOT_EXIST = JsonResponse({"error": "user does not exists"})
    PHONE_NUMBER_WAS_NOT_RECEIVED = JsonResponse({"error": "the phone number was not received from telegram bot"})
