from django.core.serializers import serialize
from django.http import HttpRequest, JsonResponse
from django.views import View

from app.internal.repositories.telegram_user_repository import TelegramUserRepository
from app.internal.services.telegram_user_service import TelegramUserService
from app.internal.transport.rest.responses import Responses


def get_tg_user_service() -> TelegramUserService:
    tg_user_repo = TelegramUserRepository()
    return TelegramUserService(tg_user_repo=tg_user_repo)


class TelegramUserView(View):
    @staticmethod
    def get(
        request: HttpRequest,
        telegram_id: int | None = None,
        tg_user_service: TelegramUserService = get_tg_user_service(),
    ) -> JsonResponse:
        if not request.user.is_superuser:
            return Responses.ACCESS_DENIED
        telegram_user = tg_user_service.get(telegram_id=telegram_id)
        if telegram_user is None:
            return Responses.USER_DOES_NOT_EXIST
        if telegram_user.phone_number is None:
            return Responses.PHONE_NUMBER_WAS_NOT_RECEIVED
        return JsonResponse({"telegram_user": serialize("json", [telegram_user])})
