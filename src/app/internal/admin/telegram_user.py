from django.contrib import admin

from app.internal.models.telegram_user import TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    """TelegramUser admin model"""

    list_display = ("telegram_id", "username", "phone_number", "updated_at", "created_at")
