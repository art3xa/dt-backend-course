from asgiref.sync import sync_to_async

from app.internal.models.telegram_user import TelegramUser
from app.internal.repositories.telegram_user_repository import TelegramUserRepository


class TelegramUserService:
    def __init__(self, tg_user_repo: TelegramUserRepository) -> None:
        self.tg_user_repo = tg_user_repo

    async def create(self, telegram_id: int, username: str) -> TelegramUser:
        return await self.tg_user_repo.create(telegram_id, username)

    def get(self, telegram_id: int) -> TelegramUser | None:
        return self.tg_user_repo.get(telegram_id)

    @sync_to_async
    def aget(self, telegram_id: int) -> TelegramUser | None:
        return self.get(telegram_id)

    async def update_phone_number(self, telegram_user: TelegramUser, phone_number: int) -> TelegramUser:
        return await self.tg_user_repo.update_phone_number(telegram_user, phone_number)
