from django.db import models
from django.db.models import BigIntegerField, CharField

from app.internal.models.base import TimeStampMixin


class TelegramUser(TimeStampMixin, models.Model):
    telegram_id = BigIntegerField(primary_key=True, unique=True, verbose_name="Telegram unique identifier for user")
    username = CharField(max_length=50, unique=True, blank=True, null=True, verbose_name="User's username")
    phone_number = CharField(max_length=50, blank=True, null=True, verbose_name="User's phone number")

    def __str__(self) -> str:
        return f"TelegramUser(id={self.telegram_id}, username={self.username})"

    class Meta:
        verbose_name = "Telegram user"
        verbose_name_plural = "Telegram users"
