from django.urls import path

from app.internal.transport.rest.handlers.default import TelegramUserView

urlpatterns = [
    path("me", TelegramUserView.as_view()),
    path("me/<int:telegram_id>", TelegramUserView.as_view()),
]
