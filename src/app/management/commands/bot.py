import asyncio
import logging
import sys

from django.core.management import BaseCommand

from app.internal.transport.bot.bot import TelegramBot
from config.config import get_settings


class Command(BaseCommand):
    def handle(self, *args, **options):
        """Start Telegram bot"""
        logging.basicConfig(level=logging.INFO, stream=sys.stdout)
        app = TelegramBot(get_settings().TELEGRAM_TOKEN)
        app.add_routers()
        asyncio.run(app.start())
