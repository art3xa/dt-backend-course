from django.contrib import admin

from app.internal.admin.admin_user import AdminUserAdmin  # noqa: F401
from app.internal.admin.telegram_user import TelegramUserAdmin  # noqa: F401

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
