CODE_FOLDERS := src/app src/config
TEST_FOLDERS :=

.PHONY: install update migrate makemigrations createsuperuser collectstatic

all: build down up

install:
	poetry install

update:
	poetry lock

migrate:
	python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	python src/manage.py runserver localhost:8000

bot:
	python src/manage.py bot

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

lint:
	poetry run ruff $(CODE_FOLDERS) $(TEST_FOLDERS) --fix
	poetry run isort $(CODE_FOLDERS) $(TEST_FOLDERS)
	poetry run flake8 --config setup.cfg $(CODE_FOLDERS) $(TEST_FOLDERS)
	poetry run black --config pyproject.toml $(CODE_FOLDERS) $(TEST_FOLDERS)

check_lint:
	poetry run ruff $(CODE_FOLDERS) $(TEST_FOLDERS)
	poetry run isort --check --diff $(CODE_FOLDERS) $(TEST_FOLDERS)
	poetry run flake8 --config setup.cfg $(CODE_FOLDERS) $(TEST_FOLDERS)
	poetry run black --check --config pyproject.toml $(CODE_FOLDERS) $(TEST_FOLDERS)

watch_lint:
	poetry run ruff $(CODE_FOLDERS) $(TEST_FOLDERS) --watch

build:
	docker compose build server

up:
	docker compose up server

down:
	docker compose down server
