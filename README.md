# Template for backend course

## Getting started

```bash
cp .env.example .env
```
- Fill TELEGRAM_TOKEN at .env with your token and other your credentials

- To create superuser
```bash
make createsuperuser
```
- To run server
```bash
make makemigrations migrate collectstatic dev
```
- To run bot
```bash
make bot
```

